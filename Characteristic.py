class Characteristic:
    stat_names = ["weapon_skill", "ballistic_skill", "strength", "toughness", "agility", "intelligence", "perception",
                  "willpower", "fellowship", "influence"]

    def __init__(self):
        # list of buffs received by the object/character
        self.buff_dict = {}

        # dict of description for the buff
        self.buff_description = {}

        #  overall stats for the object/character
        self.stats = {}

        for s in self.stat_names:
            self.stats[s] = 0

    def add_stat(self, buff_name, buff_detail, description=""):
        """
        :param buff_name: string name of the buff to be applied
        :param buff_detail: dict containing the stat name and numeric value of the buff
        :param description:  string description of the buff
        """
        for buffs in buff_detail:
            try:
                if buffs not in self.stat_names:
                    raise ValueError("OnO UwU u made a fucky wucky. '{}' is nyot a vawid stat nyame.".format(buffs))
            except ValueError:
                raise

        #  if the buff was shown before
        if buff_name in self.buff_description:
            # TODO: stacking buffs

            self.buff_dict[buff_name].update(buff_detail)
            self.buff_description[buff_name] = description

        # first time getting the buff
        else:
            self.buff_dict[buff_name] = buff_detail
            self.buff_description[buff_name] = description

        # update the total stats
        for buff in buff_detail:
            self.stats[buff] += buff_detail[buff]

            # if number overflows:
            if self.stats[buff] < 0:
                self.stats[buff] = 0
            if self.stats[buff] > 100:
                self.stats[buff] = 100

    def __str__(self):
        sb = ""
        # report the overall stats
        for s in self.stats:
            sb += "{}:{}\n".format(s, self.stats[s])
        sb += "*"*10+"\n"
        # report the stats gained from individual buffs
        for b in self.buff_dict:
            # description for the buff
            sb += b + ":" + self.buff_description[b]+"\n"
            # stats changes caused by the buff
            for bf in self.buff_dict[b]:
                sb += "{}:{}\n".format(bf, self.buff_dict[b][bf])
            sb += "- - "*4+"\n"

        sb += "="*10
        return sb
