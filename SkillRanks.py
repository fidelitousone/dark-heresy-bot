from enum import Enum


class SkillRank(Enum):
    KNOWN = 1
    TRAINED = 2
    EXPERIENCED = 3
    VETERAN = 4

