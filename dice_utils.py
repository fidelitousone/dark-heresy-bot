import random
import re


def dice_roll(iterations, dice_type):
    results = []
    for i in range(iterations):
        results.append(random.randint(1, dice_type))
    return results


def minus_characteristic():
    return sum(sorted(dice_roll(3, 10))[:2])+20


def plus_characteristic():
    return sum(sorted(dice_roll(3, 10), reverse=True)[:2])+20


def norm_characteristic():
    return sum(dice_roll(2, 10))+20


# TODO Flawed, fix
def verify_dice(roll_input):
    if re.match(r'^[1-9]+0*d[1-9]*[0-9]+(\+[1-9][0-9]*)*', roll_input):
        return True
