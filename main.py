import getopt
import json
import sys

import discord
from discord.ext import commands

from Acolyte import Acolyte
from dice_utils import *


def assign_characteristic(acolyte_obj, homeworld, plus_trait1, plus_trait2, neg_trait):
    for characteristic in acolyte_obj.characteristics.stat_names:

        if characteristic == plus_trait1 or characteristic == plus_trait2:
            acolyte_obj.characteristics.add_stat("base stat", {characteristic: plus_characteristic()},
                                                 description="base stats for acolyte from " + homeworld)
        elif characteristic == neg_trait:
            acolyte_obj.characteristics.add_stat("base stat", {characteristic: minus_characteristic()},
                                                 description="base stats for acolyte from " + homeworld)
        else:
            acolyte_obj.characteristics.add_stat("base stat", {characteristic: norm_characteristic()},
                                                 description="base stats for acolyte from " + homeworld)


def create_character(name, homeworld):
    with open("Homeworlds.json") as homeworld_json:
        homeworlds_data = json.load(homeworld_json)

    for homeworlds in homeworlds_data["Homeworlds"]:
        if homeworlds["name"] == homeworld:
            new_acolyte = Acolyte(name, homeworlds["name"])
            assign_characteristic(new_acolyte, homeworld, homeworlds["characteristic_modifiers"][0],
                                  homeworlds["characteristic_modifiers"][1], homeworlds["characteristic_modifiers"][2])
            new_acolyte.add_aptitude(homeworlds["aptitude"])
            new_acolyte.set_wounds(homeworlds["wounds"] + sum(dice_roll(1, 5)))
            if sum(dice_roll(1, 10)) >= homeworlds["fate_threshold"][1]:
                print("You have received the Emperor's Blessing! =][=")
                new_acolyte.set_fate(homeworlds["fate_threshold"][0] + 1)
            else:
                new_acolyte.set_fate(homeworlds["fate_threshold"][0])
            return new_acolyte
        else:
            pass
    print(f'Did not find home world with name {homeworld}')


# TODO Shelve functionality to store player characters
client = discord.Client()
client = commands.Bot(command_prefix='!!')


@client.event
async def on_ready():
    print(f'Logged in as {str(client)}')


@client.command()
async def homeworlds(ctx):
    string = """```Available Home Worlds:\n\n"""
    with open("Homeworlds.json") as f:
        homeworlds_data = json.load(f)
    for homeworld_list in homeworlds_data["Homeworlds"]:
        string += homeworld_list["name"]+"\n"
    string += "```"
    await ctx.send(string)


# TODO Add subtraction arithmetic
@client.command()
async def roll(ctx, arg):
    if verify_dice(arg):
        dice_parse = re.split('d', arg)
        # mod_checker = re.split(r'\+', dice_parse[1])
        mod_checker = arg.split('+')
        if "+" in arg:
            separate_roll = mod_checker[0].split("d")
            rng = dice_roll(int(separate_roll[0]), int(separate_roll[1]))
            await ctx.send(f'```Rolling {arg} {str(rng)} -> {str(sum(rng))}+{str(mod_checker[1])} = '
                           f'{(int(mod_checker[1])+sum(rng))}```')
        else:
            rng = dice_roll(int(dice_parse[0]), int(dice_parse[1]))
            await ctx.send(f'```Rolling {arg} {str(rng)} -> {str(sum(rng))}```')
    else:
        await ctx.send("❌ Not a valid dice roll")


@client.command()
async def get_id(ctx):
    await ctx.send(f'id of sender: {ctx.author.id}')


def main(argv):
    try:
        opts, args = getopt.getopt(argv, "", ["discord="])
    except getopt.GetoptError:
        print("main.py --discord=<bot token file>")
        sys.exit(2)
    for opt, arg in opts:
        if opt in "--discord":
            with open(arg) as bot_token_file:
                token = bot_token_file.read()
            client.run(token)


if __name__ == '__main__':
    main(sys.argv[1:])
