import json
import math

from Characteristic import Characteristic


def degrees_test(characteristic, roll, test_result):
    if test_result:
        if (math.floor(characteristic / 10) - math.floor(roll / 10)) == 0:
            return 1
        else:
            return math.floor(characteristic / 10) - math.floor(roll / 10)
    else:
        if (math.floor(roll / 10) - math.floor(characteristic / 10)) == 0:
            return 1
        else:
            return math.floor(roll / 10) - math.floor(characteristic / 10)


class Acolyte:
    def __init__(self, name, homeworld):
        self.name = name
        self.homeworld = homeworld
        self.wounds = 0
        self.fate = 0
        self.corruption = 0
        self.insanity = 0

        self.characteristics = Characteristic()

        self.aptitudes = {
            "weapon_skill": False,
            "ballistic_skill": False,
            "strength": False,
            "toughness": False,
            "agility": False,
            "intelligence": False,
            "perception": False,
            "willpower": False,
            "fellowship": False,
            "influence": False,
            "defense": False,
            "fieldcraft": False,
            "finesse": False,
            "general": True,
            "knowledge": False,
            "leadership": False,
            "offense": False,
            "psyker": False,
            "social": False,
            "tech": False
        }
        self.equipment = []
        self.skills = {}
        self.unspent_experience = 1000
        self.experience_spent = 0
        self.experience_total = self.unspent_experience + self.experience_spent
        self.armor = {
            "head": 0,
            "left_arm": 0,
            "right_arm": 0,
            "body": 0,
            "left_leg": 0,
            "right_leg": 0
        }

    def characteristic_bonus(self, characteristic):
        return int(((self.characteristics.stats[characteristic]) / 10) % 10)

    def add_skill(self, skill_name, rank):
        """
        :param skill_name: string name of the skill name to be added
        :param rank: enum of skill rank
        """

        if skill_name not in self.skills:
            self.skills[skill_name] = rank

    def add_aptitude(self, aptitude_name):
        """

        :param aptitude_name: name of the aptitude to add
        :return: 1 when an acolyte already possesses the aptitude
        """
        if aptitude_name in self.aptitudes and self.aptitudes[aptitude_name] is not True:
            self.aptitudes[aptitude_name] = True
        else:
            return 1

    def set_fate(self, fate):
        self.fate = fate

    def set_wounds(self, wounds):
        self.wounds = wounds

    def list_skills(self):
        print("Skills for " + self.name)
        for key in self.skills:
            print(key + " is " + self.skills[key].name)

    def list_aptitudes(self):
        print("=========================")
        for aptitude in self.aptitudes:
            if self.aptitudes[aptitude]:
                print(f'{aptitude} [X]')
            else:
                print(f'{aptitude} []')
        print("=========================")

    def add_experience(self, value):
        """

        :param value: integer value of experience to be added to acolyte
        :return:
        """

        self.unspent_experience = self.unspent_experience + int(value)

    def skill_modifier(self, name):
        if self.skills[name].value == 1:
            return 0
        elif self.skills[name].value == 2:
            return 10
        elif self.skills[name].value == 3:
            return 20
        elif self.skills[name].value == 4:
            return 30
        else:
            print("Something went wrong, this value shouldn't be here!")
            exit()

    def skill_test(self, skill, roll, modifier):
        """

        :param skill: string of what skill to test
        :param roll: integer value of the roll
        :param modifier: skill modifiers being tested
        :return (list): [0] - success or fail, [1] - original modifiers, [2] - altered modifiers
        [3] - degrees of success or failure
        """
        modifiers = modifier
        with open("skills.json") as f:
            skills_data = json.load(f)

        if skill not in self.skills:
            print("Acolyte doesn't have this skill!")
        else:
            for skill_trait in skills_data["skills"]:
                if skill_trait["name"] == skill:
                    characteristic = self.characteristics.stats[skill_trait["characteristic"]]
                    modifiers = modifiers + self.skill_modifier(skill)
                    if roll <= characteristic + modifiers:
                        degrees = degrees_test(characteristic + modifiers, roll, True)
                        f.close()
                        return True, roll, modifiers, characteristic + modifiers, degrees
                    else:
                        degrees = degrees_test(characteristic + modifiers, roll, False)
                        f.close()
                        return False, roll, modifiers, characteristic + modifiers, degrees
